import java.util.concurrent.ThreadLocalRandom;

public class Password {
	private static final int lowerAscii = 33;
	private static final int UpperAscii = 125;
	
	public static void main(String[] args){
		try{
			int pwLength = Integer.parseInt(args[0]);
			String password = genPassword(pwLength);
			System.out.println(password);
		}
		catch (Exception e) {
			
			showError(0);
		}
		
	}

	private static String genPassword(int pwLength) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < pwLength;i++){
			
			sb.append((char)(genRand(lowerAscii, UpperAscii)));
		}
		return sb.toString();
	}

	private static int genRand(int lowerascii2, int upperascii2) {
		// TODO Auto-generated method stub
		int randomNum = ThreadLocalRandom.current().nextInt(lowerascii2, upperascii2 + 1);
		//System.out.println(randomNum);
		return randomNum;
	}
	
	private static void showError(int e){
		if(e == 0){
			System.out.println("Please enter an integer between 8 and 36");
		}
	}
	
	
}
